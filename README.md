# MLMCalendarView

#### 项目介绍

仿iOS日历控件，可单选和多选 自定义颜色

#### 安装教程

1. CocoaPods安装：
         Podfile文件 输入 
         pod 'MLMCalendarView','0.0.6'
         
2. 手动安装
        下载该项目
        把文件夹中的“MLCalendar”文件夹add到自己项目中即可


#### 使用说明

1.  引入头文件  #import "MLCalendarView.h"
2. 
@interface ViewController ()

@property (nonatomic,strong)MLCalendarView * calendarView;

@end

3. 
self.calendarView = [[MLCalendarView alloc] initWithFrame:CGRectMake(0, 100, self.view.bounds.size.width, self.view.bounds.size.height - 100)];

self.calendarView.backgroundColor = [UIColor whiteColor];

self.calendarView.multiSelect = YES;//是否多选 默认NO

self.calendarView.maxTotal = 2;//最多可以选择几天 

self.calendarView.mlColor = [UIColor orangeColor];//主题颜色 默认 深红色[UIColor colorWithRed:255/255.0 green:57/255.0 blue:84/255.0 alpha:1.0]

[self.calendarView constructionUI];

__weak typeof(self) weakSelf = self;

self.calendarView.cancelBlock = ^{

[weakSelf.calendarView removeFromSuperview];
};

//单选回调
self.calendarView.selectBlock = ^(NSString *date) {


};

//多选回调
self.calendarView.multiSelectBlock = ^(NSString *beginDate, NSString *endDate, NSInteger total) {
    //beginDate 起始日期
    //endDate 结束日期
    //total 总的天数


};

[self.view addSubview:self.calendarView];

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
